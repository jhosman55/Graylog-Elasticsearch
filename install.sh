Entry this steps one to one

systemctl firewalld status
systemctl status firewalld
systemctl stop firewalld
systemctl status firewalld
sestatus
wget https://gitlab.com/jhosman55/Graylog-Elasticsearch/raw/master/selinux -O /etc/sysconfig/selinux
yum install java
rpm --import https://artifacts.elastic.co/GPG-KEY-elasticsearch
wget https://gitlab.com/jhosman55/Graylog-Elasticsearch/raw/master/elasticsearch-repo -O /etc/yum.repos.d/elasticsearch.repo
yum install elasticsearch
chkconfig --add elasticsearch
systemctl daemon-reload
systemctl enable elasticsearch.service
systemctl restart elasticsearch.service
clear
curl -X GET http://localhost:9200
read -p "If you can see the version of ElasticSearch, Press enter to continue, else restart the process"
clear
systemctl status elasticsearch
read -p "If you can see ElasticSearch running, Press enter to continue, else restart the process"
yum update
wget https://gitlab.com/jhosman55/Graylog-Elasticsearch/raw/master/mongodb_repo -O /etc/yum.repos.d/mongodb-org-3.6.repo
yum install mongodb-org
chkconfig --add mongod
systemctl daemon-reload
systemctl enable mongod.service
systemctl restart mongod.service
clear
systemctl status mongod.service
read -p "If you can see MongoDB running, Press enter to continue, else restart the process"
rpm -Uvh https://packages.graylog2.org/repo/packages/graylog-2.4-repository_latest.rpm
yum install graylog-server
yum install epel-release
yum install htop pwgen
pwgen -N 1 -s 96 > /root/password_secret
echo -n admin | sha256sum > /root/password_admin
sed  -i '1icluster.name: My Cluster Graylog' /etc/elasticsearch/elasticsearch.yml
echo "Copy this values, to continue"
tail -vn +1 /root/password_*


***

Configure now this fields in /etc/graylog/server/server.conf

password_secret = with the content of file /root/password_secret
root_password_sha2 = with the content of file /root/password_admin (remember this password is for admin on another user) 
root_timezone = YourTimeZone see: http://www.joda.org/joda-time/timezones.html
- Add this line: elasticsearch_cluster_name = My Cluster Graylog
root_email = your@email.com
- Add this line: elasticsearch_http_enabled = false
- Add this line: elasticsearch_discovery_zen_ping_unicast_hosts = 127.0.0.1:9300
elasticsearch_shards = 1
mongodb_useauth	= false
rest_listen_uri = http://0.0.0.0:9000/api/
rest_transport_uri = http://DOMAIN:9000/api/
web_enable = true
web_listen_uri = http://0.0.0.0:9000/

***

chkconfig --add graylog-server
systemctl daemon-reload
systemctl enable graylog-server.service
systemctl restart graylog-server.service
systemctl status graylog-server.service

Login in: http://IPADDRESS:9000/
